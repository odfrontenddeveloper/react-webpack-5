import React from 'react'

function App() {
    return (
        <div className="test-class">
            <h2>Welcome to React App 1</h2>
            <h3>Date : {new Date().toDateString()}</h3>
        </div>
    )
}

export default App
