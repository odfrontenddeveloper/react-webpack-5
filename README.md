!!!AT FIRST!!!

1. nvm install v16.13.1
2. nvm use v16.13.1

_ _ _ _ _ _ _ _ _ _ _ _ _
TO RUN DEV-SERVER:

1. yarn
2. yarn start

_ _ _ _ _ _ _ _ _ _ _ _ _
TO CREATE BUILD

1. yarn
2. yarn run build
_ _ _ _ _ _ _ _ _ _ _ _ _
TO RUN PRODUCTION BUILD

1. yarn
2. yarn run build
2. yarn run prod
_ _ _ _ _ _ _ _ _ _ _ _ _
BEFORE COMMIT

1. yarn run format